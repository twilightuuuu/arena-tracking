# README #

A server-client stack for tracking Hearthstone arena results, with a heavy emphasis on viewing various stats. Currently WIP.

# Planned features #

## Tracking ##

- Adding/removing single arena runs
- Adding/removing individual games

## Stats ##

- Results of arena runs
- All-time average & run count
- Daily/weekly/monthly average & run count (in mixed graph form)
- Class-class matchup win percentages
- Opponent class distribution at each win count
- The above stats, but with date filters
- All of the above, but for other users

# Installation #

## Initialization ##
```
npm install
npm run init:db
```

## Run in development mode ##
```
npm start
```
After this, open <http://localhost:3333> to view the webpage (which currently uses local storage instead of the server db).

## Build ##
```
npm run build
npm run server:production
```

## Run tests ##
```
npm run test
```
This will run some mocha-based server-side tests.
