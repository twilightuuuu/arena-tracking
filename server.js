/* eslint-disable */

const path = require('path');
const express = require('express');
const session = require('express-session');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.js');
const db = require('./server/db');
const passport = require('passport');
const apiRouter = require('./server/routing/api');
const cookieParser = require('cookie-parser');

const isDeveloping = process.env.NODE_ENV === 'development';
const port = isDeveloping ? 3000 : process.env.PORT;
const app = express();

// Configure Passport
require('./server/config/passport')(passport);

// Add Passport-related middleware with session support
app.use(cookieParser());
app.use(session({ secret: 'dudethatsawsomeletmetrythattoocoffeecoffeecoffee' }));
app.use(passport.initialize());
app.use(passport.session());


app.use('/api', apiRouter);

if (isDeveloping) {
  const compiler = webpack(config);
  const middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    contentBase: 'src',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));
  app.get('*', function response(req, res) {
    res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
    res.end();
  });
} else {
  app.use(express.static(__dirname + '/dist'));
  app.get('*', function response(req, res) {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });
}

app.on('close', function() {
    console.log("Server closed");
    db.db.close();
});

app.listen(port, '0.0.0.0', function onStart(err) {
  if (err) {
    console.log(err);
  }
  console.info('==> 🌎 Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.', port, port);
});

module.exports = app;
