import React from "react";
import ArenaGameFormContainer from "./index";

class AddArenaGameFormContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        processGame: React.PropTypes.func.isRequired,
        handleClose: React.PropTypes.func
    }

    render() {
        return (
            <ArenaGameFormContainer
                title="Add Arena Game"
                gameActionType="add"
                processGame={this.props.processGame}
                handleClose={this.props.handleClose}
            />
        );
    }
}

export default AddArenaGameFormContainer;
