// Manages ArenaGameForm"s state.

import React from "react";
import ArenaGameForm from "components/ArenaGameForm";

class ArenaGameFormContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        title: React.PropTypes.string.isRequired,
        processGame: React.PropTypes.func.isRequired,
        handleClose: React.PropTypes.func,
        removeGame: React.PropTypes.func,
        gameActionType: React.PropTypes.oneOf(["add", "remove"]),
        opponentclass: React.PropTypes.string,
        win: React.PropTypes.bool,
        wentfirst: React.PropTypes.bool
    }

    static defaultProps = {
        removeGame: () => {},
        opponentclass: "mage",
        win: true,
        wentfirst: true
    }

    state = {
        opponentclass: this.props.opponentclass,
        win: this.props.win,
        wentfirst: this.props.wentfirst
    }

    handleClassChange = event => this.setState({opponentclass: event.target.value})
    handleWinChange = event => this.setState({win: event.target.value === "win"})
    handleFirstChange = event => this.setState({wentfirst: event.target.value === "first"})
    handleSubmit = event => {
        event.preventDefault();
        this.props.processGame(this.state);
        this.props.handleClose();
    }
    handleRemove = event => {
        event.preventDefault();
        this.props.removeGame();
        this.props.handleClose();
    }

    render() {
        return (
            <ArenaGameForm 
                title={this.props.title}
                gameActionType={this.props.gameActionType}
                heroclass={this.state.opponentclass}
                win={this.state.win}
                first={this.state.wentfirst}
                onClassChange={this.handleClassChange}
                onWinChange={this.handleWinChange}
                onFirstChange={this.handleFirstChange}
                onSubmit={this.handleSubmit}
                onRemoveSubmit={this.handleRemove}
            />
        );
    }
}

export default ArenaGameFormContainer;
