import React from "react";
import ArenaGameFormContainer from "./index";

class EditArenaGameFormContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        processGame: React.PropTypes.func.isRequired,
        removeGame: React.PropTypes.func.isRequired,
        handleClose: React.PropTypes.func,
        opponentclass: React.PropTypes.string.isRequired,
        win: React.PropTypes.bool.isRequired,
        wentfirst: React.PropTypes.bool.isRequired 
    }

    render() {
        return (
            <ArenaGameFormContainer
                title="Edit Arena Game"
                gameActionType="remove"
                processGame={this.props.processGame}
                removeGame={this.props.removeGame}
                opponentclass={this.props.opponentclass}
                win={this.props.win}
                wentfirst={this.props.wentfirst}
                handleClose={this.props.handleClose}
            />
        );
    }
}

export default EditArenaGameFormContainer;
