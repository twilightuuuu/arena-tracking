// Displays info about a single arena run.

import React from "react";
import Arena from "components/Arena";

const ArenaContainer = (props) => {
    return (
        <Arena {...props} />
    );
};

export default ArenaContainer;
