// The container for the NewArenaGame component.
// Is connected to the store to perform new game adding actions.

import React from "react";
import {connect} from "react-redux";
import * as ArenaListActions from "app/actions/ArenaListActions";
import ModalPopupContainer from "containers/ModalPopupContainer";
import NewArenaGame from "components/ArenaGame/NewArenaGame";
import AddArenaGameFormContainer from "containers/ArenaGameFormContainer/AddArenaGameFormContainer";
import Console from "app/helpers/Console";

class NewArenaGameContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        arenaId: React.PropTypes.number.isRequired,
        addGame: React.PropTypes.func.isRequired
    }

    addNewGame = (gameResult) => {
        this.props.addGame(this.props.arenaId, gameResult);
    }


    render() {
        const wrapper = (<NewArenaGame />);
        Console.log("wrapper:", wrapper);
        const content = (
            <AddArenaGameFormContainer
                processGame={this.addNewGame}
            />
        );
        Console.log("content:", content);

        return (
            <ModalPopupContainer
                wrapper={wrapper}
                content={content}
            />
        );
    }
}

export default connect(
    undefined,
    ArenaListActions
)(NewArenaGameContainer);
