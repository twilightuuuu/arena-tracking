// A container holding the ArenaList info.

import React from "react";
import {connect} from "react-redux";
import * as ArenaListActions from "app/actions/ArenaListActions";
import ArenaTrendGraph from "components/ArenaTrendGraph";
import SearchCriteriaContainer from "containers/SearchCriteriaContainer";
import ArenaList from "components/ArenaList";
import StatSummary from "components/StatSummary";
import ReactPaginate from "react-paginate";
import Console from "app/helpers/Console";
import styles from "./styles.css";

class ArenaListContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        loadInitialArenaList: React.PropTypes.func.isRequired,
        setCurrentPage: React.PropTypes.func.isRequired,
        graphData: React.PropTypes.array,
        stats: React.PropTypes.object,
        arenasPerPage: React.PropTypes.number.isRequired,
        currentPage: React.PropTypes.number.isRequired,
        arenas: React.PropTypes.array
    }

    componentDidMount() {
        this.props.loadInitialArenaList();
    }

    onPageClick = data => {
        Console.log("page click", data);
        this.props.setCurrentPage(data.selected);
    };

    render() {
        return (
            <div>
                <ArenaTrendGraph
                    data={this.props.graphData}
                />
                <SearchCriteriaContainer />
                <StatSummary 
                    stats={this.props.stats}
                />
                <ReactPaginate
                    pageCount={Math.ceil(Math.max(1, this.props.stats.total) / this.props.arenasPerPage)}
                    forcePage={this.props.currentPage}
                    pageRangeDisplayed={5}
                    marginPagesDisplayed={2}
                    containerClassName={styles.paginate}
                    onPageChange={this.onPageClick}
                />
                <ArenaList 
                    arenas={this.props.arenas}
                    isFirstPage={this.props.currentPage === 0}
                />
            </div>
        );
    }
}

export default connect(
    (store) => ({
        ...store.ArenaList
    }),
    ArenaListActions
)(ArenaListContainer);
