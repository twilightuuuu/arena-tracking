import React from "react";
import styles from "./styles.css";
import {connect} from "react-redux";
import MenuContainer from "containers/MenuContainer";
import DevApp from "components/DevApp";
import Console from "app/helpers/Console";

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        location: React.PropTypes.object.isRequired,
        children: React.PropTypes.object
    }

    render() {
        Console.log("App props:", this.props);
        return (
            <div className={styles.layout}>
                <MenuContainer 
                    currentRoute={this.props.location.pathname}
                />
                <div className={styles.main}>
                    {this.props.children}
                </div>
                <DevApp />
            </div>
        );
    }
}

export default connect(
    (store, ownProps) => ({
        routes: ownProps.routes
    }),
    undefined
)(App);
