// Manages NewArenaForm"s state.

import React from "react";
import NewArenaForm from "components/NewArenaForm";
import Console from "app/helpers/Console";

class NewArenaFormContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        addArena: React.PropTypes.func.isRequired,
        handleClose: React.PropTypes.func
    }

    static defaultProps = {
        handleClose: () => {}
    }

    state = {
        ownclass: "mage"
    }

    handleClassChange = event => this.setState({ownclass: event.target.value});
    handleSubmit = event => {
        event.preventDefault();
        Console.log("New Arena", this.state.ownclass);
        this.props.addArena(this.state);
        this.props.handleClose();
    }

    render() {
        return (
            <NewArenaForm
                onSubmit={this.handleSubmit}
                heroclass={this.ownclass}
                onClassChange={this.handleClassChange}
            />
        );
    }
}

export default NewArenaFormContainer;
