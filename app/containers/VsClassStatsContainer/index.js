import React from "react";
import AreaWithHeader from "components/AreaWithHeader";
import {getVsClassStats} from "app/helpers/ArenaData";
import OpponentClassDistGraph from "components/OpponentClassDistGraph";
import ClassMatchupTable from "components/ClassMatchupTable";

class VsClassStatsContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined
        };
    }

    componentDidMount() {
        this.setState({
            ...getVsClassStats()
        });
    }

    render() {
        return (
            <div>
                <div>
                    <AreaWithHeader
                        header="Opponent class distribution"
                    >
                        <OpponentClassDistGraph
                            data={this.state.winDistribution}
                        />
                    </AreaWithHeader>
                </div>
                <div>
                    <AreaWithHeader
                        header="Class matchups"
                    >
                        <ClassMatchupTable
                            data={this.state.classMatchups}
                        />
                    </AreaWithHeader>
                </div>
            </div>
        );
    }
}

export default VsClassStatsContainer;
