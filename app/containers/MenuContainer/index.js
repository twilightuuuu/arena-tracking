// The menu container.
// Contains menu items & user session related items.
import React from "react";
import styles from "./styles.css";
import MenuLink from "components/MenuLink";
import UserSessionContainer from "containers/UserSessionContainer";

class MenuContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        currentRoute: React.PropTypes.string.isRequired
    };

    render() {
        console.log(this.props);
        return (
            <div className={styles.container}>
                <UserSessionContainer />
                <div className="pure-menu">
                    <ul className="pure-menu-list">
                        <MenuLink
                            to="/"
                            label="Home"
                            isActive={this.props.currentRoute === "/"}
                        />
                        <MenuLink
                            to="/stats"
                            label="Stats"
                            isActive={this.props.currentRoute.startsWith("/stats")}
                        />
                    </ul>
                </div>
            </div>
        );
    }
}

export default MenuContainer;
