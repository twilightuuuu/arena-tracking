import React from "react";
import {connect} from "react-redux";
import * as ArenaListActions from "app/actions/ArenaListActions";
import ArenaGame from "components/ArenaGame";
import EditArenaGameFormContainer from "containers/ArenaGameFormContainer/EditArenaGameFormContainer";
import ModalPopupContainer from "containers/ModalPopupContainer";

class ArenaGameContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        arenaId: React.PropTypes.number.isRequired,
        editGame: React.PropTypes.func.isRequired,
        gameIndex: React.PropTypes.number.isRequired,
        opponentclass: React.PropTypes.string.isRequired,
        removeGame: React.PropTypes.func.isRequired,
        win: React.PropTypes.bool.isRequired,
        wentfirst: React.PropTypes.bool.isRequired
    }

    state = {
        opponentclass: this.props.opponentclass,
        win: this.props.win,
        wentfirst: this.props.wentfirst
    }

    editGame = (gameResult) => {
        this.props.editGame(this.props.arenaId, this.props.gameIndex, gameResult);
    }
    removeGame = () => {
        this.props.removeGame(this.props.arenaId, this.props.gameIndex);
    }

    render() {
        const wrapper = (
            <ArenaGame 
                opponentclass={this.props.opponentclass}
                win={this.props.win}
                wentfirst={this.props.wentfirst}
            />
        );
        const content = (
            <EditArenaGameFormContainer
                processGame={this.editGame}
                removeGame={this.removeGame}
                opponentclass={this.props.opponentclass}
                win={this.props.win}
                wentfirst={this.props.wentfirst}
            />
        );

        return (
            <ModalPopupContainer
                wrapper={wrapper}
                content={content}
            />
        );
    }
}

export default connect(
    undefined,
    ArenaListActions
)(ArenaGameContainer);
