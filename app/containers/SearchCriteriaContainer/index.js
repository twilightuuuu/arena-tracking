// A container that holds the current search criteria state.

import React from "react";
import SearchCriteria from "components/SearchCriteria";

class SearchCriteriaContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SearchCriteria />
        );
    }
}

export default SearchCriteriaContainer;
