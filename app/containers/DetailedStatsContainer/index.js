import React from "react";
import DetailedStatsMenu from "components/DetailedStatsMenu";

class DetailedStatsContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        location: React.PropTypes.object.isRequired,
        children: React.PropTypes.object
    }

    render() {
        return (
            <div>
                <DetailedStatsMenu 
                    currentRoute={this.props.location.pathname}
                />
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default DetailedStatsContainer;
