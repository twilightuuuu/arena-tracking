import React from "react";
import {connect} from "react-redux";
import * as ArenaListActions from "app/actions/ArenaListActions";
import NewArena from "components/NewArena";
import NewArenaFormContainer from "containers/NewArenaFormContainer";
import ModalPopupContainer from "containers/ModalPopupContainer";

class NewArenaContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        addArena: React.PropTypes.func
    }

    addArena = (arena) => {
        this.props.addArena(arena);
    }

    render() {
        const wrapper = (
            <NewArena />
        );
        const content = (
            <NewArenaFormContainer
                addArena={this.addArena}
            />
        );
    
        return (
            <ModalPopupContainer
                wrapper={wrapper}
                content={content}
            />
        );
    }
}

export default connect(
    undefined,
    ArenaListActions
)(NewArenaContainer);
