// Containers user info if the user is logged in,
// and a register/login combo if the user isn't logged in.
import React from "react";
import ModalPopupContainer from "containers/ModalPopupContainer";
import LoginButton from "components/LoginButton";
import UserLoginContainer from "containers/UserLoginContainer";

class UserSessionContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const wrapper = (
            <LoginButton />
        );
        const content = (
            <UserLoginContainer 
                tryLogin={()=>{}}
            />
        );

        return (
            <ModalPopupContainer
                wrapper={wrapper}
                content={content}
            />
        );
    }
}

export default UserSessionContainer;
