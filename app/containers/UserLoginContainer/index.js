import React from "react";
import UserLoginForm from "components/UserLoginForm";

class UserLoginContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        tryLogin: React.PropTypes.func.isRequired,
        handleClose: React.PropTypes.func
    }

    state = {
        user: "",
        password: ""
    }

    handleUserChange = event => this.setState({
        user: event.target.value
    });
    handlePasswordChange = event => this.setState({
        password: event.target.value
    });
    handleSubmit = event => {
        event.preventDefault();
        this.props.tryLogin(this.state.user, this.state.password);
    };

    render() {
        return (
            <UserLoginForm
                onSubmit={this.handleSubmit}
                onUserChange={this.handleUserChange}
                onPasswordChange={this.handlePasswordChange}
            />
        );
    }
}

export default UserLoginContainer;
