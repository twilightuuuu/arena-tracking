// A container with a outer wrapper & popup content.

import React from "react";
import {ModalContainer, ModalDialog} from "react-modal-dialog";

class ModalPopupContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        wrapper: React.PropTypes.element,
        content: React.PropTypes.element
    }

    static defaultProps = {
        content: <div></div>
    }

    state = {
        showModal: false
    }

    popup = () => {
        if (this.state.showModal) {
            const content = React.cloneElement(
                this.props.content, {
                    handleClose: this.handleClose
                }
            );

            return (
                <ModalContainer
                    zIndex={2000}
                    onClose={this.handleClose}
                >
                    <ModalDialog
                        style={{maxWidth:"800px"}}
                        onClose={this.handleClose}
                    >
                        {content}
                    </ModalDialog>
                </ModalContainer>
            );
        }
        else {
            return <div></div>;
        }
    }

    handleClick = () => this.setState({showModal:true})
    handleClose = () => this.setState({showModal:false})

    render() {
        return React.cloneElement(
            this.props.wrapper, {
                handleClick: this.handleClick
            },
            this.popup()
        );
    }
}

export default ModalPopupContainer;
