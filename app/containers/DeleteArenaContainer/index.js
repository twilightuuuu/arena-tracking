import React from "react";
import {connect} from "react-redux";
import * as ArenaListActions from "app/actions/ArenaListActions";
import ModalPopupContainer from "containers/ModalPopupContainer";
import DeleteArena from "components/DeleteArena";
import ConfirmForm from "components/ConfirmForm";

class DeleteArenaContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        arenaId: React.PropTypes.number.isRequired,
        deleteArena: React.PropTypes.func
    };

    deleteArena = () => {
        this.props.deleteArena(this.props.arenaId);
    }

    render() {
        const wrapper = (
            <DeleteArena />
        );
        const content = (
            <ConfirmForm 
                message="Really delete this arena?"
                onSubmit={this.deleteArena}
            />
        );

        return (
            <ModalPopupContainer
                wrapper={wrapper}
                content={content}
            />
        );
    }
}

export default connect(
    undefined,
    ArenaListActions
)(DeleteArenaContainer);
