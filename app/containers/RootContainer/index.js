// The outermost component.
// Everything that needs the store must go inside here.
import React from "react";
import {Provider} from "react-redux";
import AppContainer from "containers/AppContainer";
import {Router, Route, IndexRoute, browserHistory} from "react-router";
import {syncHistoryWithStore} from "react-router-redux";
import ArenaListContainer from "containers/ArenaListContainer";
import DetailedStatsContainer from "containers/DetailedStatsContainer";
import VsClassStatsContainer from "containers/VsClassStatsContainer";

const RootContainer = ({store}) => {
    const history = syncHistoryWithStore(browserHistory, store);
    return (
        <Provider store={store}>
            <Router history={history}>
                <Route path="/" component={AppContainer}>
                    <IndexRoute component={ArenaListContainer} />
                    <Route path="stats" component={DetailedStatsContainer}>
                        <Route path="vs-class" component={VsClassStatsContainer} />
                    </Route>
                </Route>
            </Router>
        </Provider>
    );
};

RootContainer.propTypes = {
    store: React.PropTypes.object.isRequired
};

export default RootContainer;
