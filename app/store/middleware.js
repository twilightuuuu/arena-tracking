// Common middleware to apply on the store.

import {applyMiddleware} from "redux";
// import * as ArenaData from "app/helpers/ArenaData";
import Thunk from "redux-thunk";

// Saves the current arena data to localStorage upon detecting a change to the ArenaList state.
// const saveArenaData = store => next => action => {
//    const prevArenaData = store.getState().ArenaList;
//    let result = next(action);
//   const curArenaData = store.getState().ArenaList;
//   if (prevArenaData != curArenaData) {
//       ArenaData.setArenaList(curArenaData);
//   }
//
//   return result;
//};

export default applyMiddleware(
    Thunk
);
