import {createStore, compose} from "redux";
import rootReducer from "../reducers";
import DevTools from "components/DevTools";
import middleware from "./middleware";

const enhancer = compose(
    middleware,
    DevTools.instrument()
);

export default (initialState) => {
    const store = createStore(rootReducer, initialState, enhancer);

    if (module.hot) {
        module.hot.accept("../reducers", () => store.replaceReducer(require("../reducers")));
    }

    return store;
};
