// Action types related to the ArenaList.

export const LOAD = "ARENALIST_LOAD";
export const ADD_GAME = "ARENALIST_ADD_GAME";
export const EDIT_GAME = "ARENALIST_EDIT_GAME";
export const REMOVE_GAME = "ARENALIST_REMOVE_GAME";
export const ADD_ARENA = "ARENALIST_ADD_ARENA";
export const REMOVE_ARENA = "ARENALIST_REMOVE_ARENA";
export const SET_CURRENT_PAGE = "ARENALIST_SET_CURRENT_PAGE";
