// An object combining all action types.
// We do this in order to 'namespace' the actions.

import * as ArenaListActionTypes from "./ArenaListActionTypes"; 

export default {
    ArenaList: ArenaListActionTypes
};
