// Functions for getting/setting arena data of a user.

import Lockr from "lockr";
import testData from "app/actions/arenalist-testdata";
import Console from "app/helpers/Console";

const GRAPH_DATAPOINTS = 15;
const CLASSES = ["mage", "warlock", "priest", "paladin", "hunter", "warrior", "shaman", "druid", "rogue"];

const noPriorClassesProb = (classTotal, priorClassCount, choices) => {
    let prob = 1;
    for (let i = 0 ; i < choices ; i++) {
        prob *= (classTotal - priorClassCount - i) / (classTotal - i);
    }
    return prob;
};

const classAppearProb = (classTotal, choices) => {
    return 1 - noPriorClassesProb(classTotal, 1, choices);
};

function generateOptimalClassProbs(classTotal, choices) {
    let classProbs = [];
    for (let i = 0 ; i < classTotal - choices + 1 ; i++) {
        classProbs.push(noPriorClassesProb(classTotal, i, choices) * classAppearProb(classTotal - i, choices));
    }
    // choices - 1 classes don"t get picked at all.
    for (let i = 0 ; i < choices - 1 ; i++) {
        classProbs.push(0);
    }
    return classProbs;
}

// The list of probabilities of choosing a class,
// assuming the best class from the selections always
// gets chosen first.
const optimalClassProbs = generateOptimalClassProbs(9, 3);

Console.log("optimalClassProbs:", optimalClassProbs);

function getFullArenaList() {
    return Lockr.get("ArenaList", testData);
}

function setArenaList(arenaList) {
    Lockr.set("ArenaList", arenaList);
}

function arenaToWins(arena) {
    return arena.games.map(game => game.win? 1:0).reduce((x,y) => x+y, 0);
}

function arenasToWins(arenas) {
    return arenas.map(arenaToWins);
}

function arenaWinAvg(arenas) {
    if (arenas.length > 0) {
        return arenasToWins(arenas).reduce((x,y) => x+y, 0)/arenas.length;
    }
    else {
        return 0;
    }
}

function classArenaWinAvg(arenas, myClass) {
    return arenaWinAvg(arenas.filter(arena => arena.ownclass === myClass));
}

function optimalArenaWinAvg(arenas) {
    // Get a sorted list of class averages,
    // so that the highest average comes first.
    const classesAvg = ["mage", "warrior", "priest", "druid", "paladin", "warlock", "rogue", "shaman", "hunter"].map(myClass => classArenaWinAvg(arenas, myClass)).sort((x,y) => y-x);

    Console.log("classesAvg:", classesAvg);

    // Return the sum of (classAvg * classProb).
    return classesAvg.map((classAvg, index) => classAvg * optimalClassProbs[index]).reduce((x,y) => x+y, 0); 
}

function createGraphData(arenas) {
    // Get a slice of the arenas we need to calculate the N-run average
    // of the must recent datapoints, and calculate the required values.
    const recentArenasData = arenas.slice(0, GRAPH_DATAPOINTS+(5-1));

    // Create the datapoints.
    // Since we want the most recent result to go on the right-hand side,
    // we will reverse the mapped datapoints before returning.
    return recentArenasData.map((arena, index) => ({
        wins: arenaToWins(arena),
        average5: arenaWinAvg(recentArenasData.slice(index, index+5)) 
    })).reverse();
}

function createStats(fullArenaList) {
    return {
        total: fullArenaList.arenas.length,
        average: {
            all: arenaWinAvg(fullArenaList.arenas),
            optimal: optimalArenaWinAvg(fullArenaList.arenas)
        }
    };
}

function oppClassDistributionForWins(fullArenaList) {
    let data = new Array(12).fill(0).map((x, index) => {
        let values = {wins: index};
        for (let className of CLASSES) {
            values[className] = 0;
        }
        return values;
    });

    for (let arena of fullArenaList.arenas) {
        let currentWins = 0;
        for (let game of arena.games) {
            data[currentWins][game.opponentclass]++;
            if (game.win) {
                currentWins++;
            }
        }
    }

    return data;
}

function classMatchups(fullArenaList) {
    let data = CLASSES.reduce((object, className) => {
        object[className] = CLASSES.reduce((innerObject, innerClassName) => {
            innerObject[innerClassName] = [0,0];
            return innerObject;
        }, {});
        return object;
    }, {});

    for (let arena of fullArenaList.arenas) {
        for (let game of arena.games) {
            data[arena.ownclass][game.opponentclass][game.win? 0:1]++;
        }
    }

    return data;
}

export function addArena(arena) {
    let arenaList = getFullArenaList();
    const nowDate = Date.now();
    arenaList.arenas.splice(0, 0, {
        id: arenaList.arenas.length > 0 ? arenaList.arenas[0].id + 1 : 0,
        date: nowDate,
        ownclass: arena.ownclass,
        games: []
    });
    arenaList.total = arenaList.total + 1;

    setArenaList(arenaList);
}

export function deleteArena(arenaId) {
    let arenaList = getFullArenaList();
    arenaList.arenas = arenaList.arenas.filter(arena => arena.id !== arenaId);
    arenaList.total = arenaList.total - 1;

    setArenaList(arenaList);
}

export function addGame(arenaId, newGame) {
    let arenaList = getFullArenaList();
    arenaList.arenas = arenaList.arenas.map(arena => {
        if (arena.id === arenaId) {
            arena.games.push(newGame);
        }
        return arena;
    });

    setArenaList(arenaList);
}

export function editGame(arenaId, gameIndex, newGame) {
    let arenaList = getFullArenaList();
    arenaList.arenas = arenaList.arenas.map(arena => {
        if (arena.id === arenaId) {
            arena.games[gameIndex] = newGame;
        }
        return arena;
    });

    setArenaList(arenaList);
}

export function removeGame(arenaId, gameIndex) {
    let arenaList = getFullArenaList();
    arenaList.arenas = arenaList.arenas.map(arena => {
        if (arena.id === arenaId) {
            arena.games.splice(gameIndex, 1);
        }
        return arena;
    });

    setArenaList(arenaList);
}


export function getArenaList(page = 0, arenasPerPage) {
    let fullArenaList = getFullArenaList();
    let showArenaList = fullArenaList.arenas.slice(page * arenasPerPage, (page+1) * arenasPerPage);
    return {
        arenas: showArenaList,
        graphData: createGraphData(fullArenaList.arenas),
        stats: createStats(fullArenaList)
    };
}

export function getVsClassStats() {
    let fullArenaList = getFullArenaList();
    return {
        winDistribution: oppClassDistributionForWins(fullArenaList),
        classMatchups: classMatchups(fullArenaList)
    };
}
