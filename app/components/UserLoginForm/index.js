import React from "react";
import PopupForm from "components/PopupForm";

const UserLoginForm = (props) => {
    return (
        <PopupForm
            title="Login"
            onSubmit={props.onSubmit}
        >
            <button type="submit" className="pure-button pure-button-primary">
                Login
            </button>
        </PopupForm>
    );
};

UserLoginForm.propTypes = {
    onSubmit: React.PropTypes.func.isRequired
};

export default UserLoginForm;
