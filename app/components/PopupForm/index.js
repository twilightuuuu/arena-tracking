// Basic template for popup forms.

import React from "react";

const PopupForm = (props) => {
    return (
        <div>
            <div>
                {props.title}
            </div>
            <form className="pure-form pure-form-stacked" onSubmit={props.onSubmit}>
                {props.children}
            </form>
            {props.extraContent}
        </div>
    );
};

PopupForm.propTypes = {
    title: React.PropTypes.string.isRequired,
    onSubmit: React.PropTypes.func,
    extraContent: React.PropTypes.node,
    children: React.PropTypes.object
};

PopupForm.defaultProps = {
    onSubmit: undefined,
    children: undefined,
    extraContent: undefined
};

export default PopupForm;
