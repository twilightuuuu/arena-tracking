import React from "react";

const ClassSelect = (props) => {
    return (
        <fieldset>
            <label htmlFor="heroclass">Class</label>
            <select id="heroclass" name={props.name} value={props.value} onChange={props.onChange}>
                <option value="mage">Mage</option>
                <option value="rogue">Rogue</option>
                <option value="paladin">Paladin</option>
                <option value="shaman">Shaman</option>
                <option value="warlock">Warlock</option>
                <option value="warrior">Warrior</option>
                <option value="druid">Druid</option>
                <option value="hunter">Hunter</option>
                <option value="priest">Priest</option>
            </select>
        </fieldset>
    );
};

ClassSelect.propTypes = {
    name: React.PropTypes.string.isRequired,
    value: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func.isRequired
};

export default ClassSelect;
