// Shows a button to login.

import React from "react";

const LoginButton = (props) => {
    return (
        <div>
            <button className="pure-button" onClick={props.handleClick}>
                Login
                {props.children}
            </button>
        </div>
    );
};

LoginButton.propTypes = {
    handleClick: React.PropTypes.func,
    children: React.PropTypes.object
};

export default LoginButton;

