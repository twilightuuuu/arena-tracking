// A form for filling out information for a new arena.

import React from "react";
import PopupForm from "components/PopupForm";
import ClassSelect from "components/PopupForm/parts/ClassSelect";

const NewArenaForm = (props) => {
    return (
        <PopupForm
            title="Add New Arena Run"
            onSubmit={props.onSubmit}
        >
            <ClassSelect
                name="heroclass"
                value={props.heroclass}
                onChange={props.onClassChange}
            />
            <button type="submit" className="pure-button pure-button-primary">
                Add Arena
            </button>
        </PopupForm>
    );
};

NewArenaForm.propTypes = {
    onSubmit: React.PropTypes.func.isRequired,
    heroclass: React.PropTypes.string.isRequired,
    onClassChange: React.PropTypes.func.isRequired
};

export default NewArenaForm;
