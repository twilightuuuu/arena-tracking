import React from "react";
import PopupForm from "components/PopupForm";

const ConfirmForm = (props) => {
    return (
        <PopupForm
            title="Confirm your choice"
            onSubmit={(event) => {event.preventDefault(); props.onSubmit(); props.handleClose();}}
        >
            <div>
                {props.message}
            </div>
            <button type="submit" className="pure-button pure-button-primary">
                Yes
            </button>
        </PopupForm>
    );
};

ConfirmForm.propTypes = {
    message: React.PropTypes.string.isRequired,
    onSubmit: React.PropTypes.func.isRequired,
    handleClose: React.PropTypes.func
};

ConfirmForm.defaultProps = {
    handleClose: () => {}
};

export default ConfirmForm;
