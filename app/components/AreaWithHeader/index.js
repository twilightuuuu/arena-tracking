import React from "react";
import styles from "./styles.css";

const AreaWithHeader = (props) => {
    return (
        <div>
            <div className={styles.header}>
                {props.header}
            </div>
            {props.children}
        </div>
    );
};

AreaWithHeader.propTypes = {
    header: React.PropTypes.string.isRequired,
    children: React.PropTypes.object
};

export default AreaWithHeader;
