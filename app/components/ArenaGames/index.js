// Shows a list of games for a single arena run.

import React from "react";
import ArenaGameContainer from "containers/ArenaGameContainer";
import NewArenaGameContainer from "containers/NewArenaGameContainer";
import Console from "app/helpers/Console";

const ArenaGames = (props) => {
    Console.log("ArenaGames:", props);
    return (
        <div className="pure-g">
            {props.games.map((game, index) => (<ArenaGameContainer key={index} gameIndex={index} arenaId={props.arenaId} {...game}/>))}
            <NewArenaGameContainer arenaId={props.arenaId}/>
        </div>
    );
};

ArenaGames.propTypes = {
    games: React.PropTypes.array.isRequired,
    arenaId: React.PropTypes.number.isRequired
};

export default ArenaGames;
