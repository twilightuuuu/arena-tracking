// A component representing a visualization of recent arena win counts.

import React from "react";
import {LineChart, Line, XAxis, YAxis, Legend, Tooltip} from "recharts";
import styles from "./styles.css";
import Console from "app/helpers/Console";

class DisplayTooltip extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        active: React.PropTypes.bool,
        payload: React.PropTypes.array
    }

    render() {
        if (this.props.active) {
            return (
                <div className={styles.tooltipBox}>
                    <p>
                        {this.props.payload[0].payload.wins} wins
                    </p>
                    <p>
                        5-arena avg: {this.props.payload[0].payload.average5}
                    </p>
                </div>
            );
        }
        else {
            return undefined;
        }
    }
}

const ArenaTrendGraph = (props) => {
    Console.log("graph data:", props.data);
    return (
        <div className={styles.box}>
            <LineChart
                width={700}
                height={190}
                data={props.data}
            >
                <XAxis 
                    tick={false}
                />
                <YAxis 
                    domain={[0,12]}
                />
                <Tooltip 
                    content={<DisplayTooltip />}
                />
                <Legend />
                <Line
                    dataKey="average5"
                    stroke="#3399cc"
                    isAnimationActive={false}
                />
                <Line
                    dataKey="wins"
                    stroke="#cc9933"
                    isAnimationActive={false}
                />
            </LineChart>
        </div>
    );
};

ArenaTrendGraph.propTypes = {
    data: React.PropTypes.array.isRequired
};

export default ArenaTrendGraph;
