// A component representing a single menu item.

import React from "react";
import styles from "./styles.css";
import {Link} from "react-router";
import classNames from "classnames";

const MenuLink = ({to, label, isActive}) => {
    return (
        <li className={classNames("pure-menu-item", styles.linkBox, {[styles.active]: isActive})}>
            <Link className={styles.link} to={to}>{label}</Link>
        </li>
    );
};

MenuLink.propTypes = {
    to: React.PropTypes.string.isRequired,
    label: React.PropTypes.string.isRequired,
    isActive: React.PropTypes.bool.isRequired
};

export default MenuLink;
