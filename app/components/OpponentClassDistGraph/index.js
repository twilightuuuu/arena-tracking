import React from "react";
import {AreaChart, Area, XAxis, YAxis, Tooltip} from "recharts";

const classProps = [
    ["mage", "#4CA6CC"],
    ["warrior", "#8D0F00"],
    ["shaman", "#011784"],
    ["rogue", "#4B4C47"],
    ["paladin", "#A98E00"],
    ["hunter", "#006E00"],
    ["druid", "#703505"],
    ["warlock", "#7623AD"],
    ["priest", "#C7C19E"]
];

const OpponentClassDistGraph = (props) => {
    return (
        <AreaChart
            width={800}
            height={300}
            data={props.data}
            stackOffset="expand"
        >
            <XAxis dataKey="wins" label="Wins"/>
            <YAxis />
            <Tooltip />
            {classProps.map(props => (
                <Area
                    key={props[0]}
                    dataKey={props[0]}
                    stackId="1"
                    fill={props[1]}
                    isAnimationActive={false}
                />
            ))}
        </AreaChart>
    );
};

OpponentClassDistGraph.propTypes = {
    data: React.PropTypes.array
};

export default OpponentClassDistGraph;
