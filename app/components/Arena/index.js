// Displays info about a single arena run.

import React from "react";
import ArenaHeader from "components/ArenaHeader";
import ArenaGames from "components/ArenaGames";

const getRecord = (games) => ({
    wins: games.map(game => game.win ? 1:0).reduce((a,b) => a+b, 0),
    losses: games.map(game => game.win ? 0:1).reduce((a,b) => a+b, 0)
});

const Arena = (props) => {
    return (
        <div>
            <ArenaHeader arenaId={props.id} date={props.date} ownclass={props.ownclass} {...getRecord(props.games)}/>
            <ArenaGames games={props.games} arenaId={props.id}/>
        </div>
    );
};

Arena.propTypes = {
    date: React.PropTypes.number.isRequired,
    ownclass: React.PropTypes.string.isRequired,
    games: React.PropTypes.array.isRequired,
    id: React.PropTypes.number.isRequired
};

export default Arena;
