// A form for filling out arena game information.

import React from "react";
import PopupForm from "components/PopupForm";
import ClassSelect from "components/PopupForm/parts/ClassSelect";

const AddGameSubmit = (
    <button type="submit" className="pure-button pure-button-primary">Add Game</button>
);

const EditGameSubmit = (
    <button type="submit" className="pure-button pure-button-primary">Edit Game</button>
);

const RemoveGameSubmitForm = onRemoveSubmit => (
    <form onSubmit={onRemoveSubmit}>
        <button type="submit" className="pure-button pure-button-primary">Remove Game</button>
    </form>
);

const ArenaGameForm = (props) => {
    const submit = props.gameActionType === "add" ? AddGameSubmit : EditGameSubmit;
    const remove = props.gameActionType === "remove" ? RemoveGameSubmitForm(props.onRemoveSubmit) : undefined;
    return (
        <PopupForm
            title={props.title}
            onSubmit={props.onSubmit}
            extraContent={remove}
        >
            <ClassSelect
                name="heroclass"
                value={props.heroclass}
                onChange={props.onClassChange}
            />
            <fieldset>
                <legend>Turn order</legend>
                <label htmlFor="arenaturn-first" className="pure-radio">
                    <input type="radio" id="arenaturn-first" name="arenaturn" value="first" checked={props.first} onChange={props.onFirstChange}/>
                    First
                </label>
                <label htmlFor="arenaturn-second" className="pure-radio">
                    <input type="radio" id="arenaturn-second" name="arenaturn" value="second" checked={!props.first} onChange={props.onFirstChange}/>
                    Second
                </label>
            </fieldset>
            <fieldset>
                <legend>Result</legend>
                <label htmlFor="arenawin-win" className="pure-radio">
                    <input type="radio" id="arenawin-win" name="arenawin" value="win" checked={props.win} onChange={props.onWinChange}/>
                    Win
                </label>
                <label htmlFor="arenawin-lose" className="pure-radio">
                    <input type="radio" id="arenawin-lose" name="arenawin" value="lose" checked={!props.win} onChange={props.onWinChange}/>
                    Lose
                </label>
            </fieldset>
            {submit}
        </PopupForm>
    );
};

ArenaGameForm.propTypes = {
    title: React.PropTypes.string.isRequired,
    gameActionType: React.PropTypes.oneOf(["add", "remove"]),
    heroclass: React.PropTypes.string.isRequired,
    win: React.PropTypes.bool.isRequired,
    first: React.PropTypes.bool.isRequired,
    onClassChange: React.PropTypes.func.isRequired,
    onWinChange: React.PropTypes.func.isRequired,
    onFirstChange: React.PropTypes.func.isRequired,
    onSubmit: React.PropTypes.func.isRequired,
    onRemoveSubmit: React.PropTypes.func.isRequired
};

export default ArenaGameForm;
