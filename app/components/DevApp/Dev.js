import React from "react";
import DevTools from "components/DevTools";

const DevApp = () => {
    return (
        <DevTools />
    );
};

export default DevApp;
