import React from "react";

// We don't want the DevApp to show up in production.
const DevApp = () => {
    return (<div></div>);
};

export default DevApp;
