// Shows a button to add a new arena.

import React from "react";

const NewArena = (props) => {
    return (
        <div>
            <button className={"pure-button"} onClick={props.handleClick}>
                New Arena
                {props.children}
            </button>
        </div>
    );
};

NewArena.propTypes = {
    handleClick: React.PropTypes.func,
    children: React.PropTypes.object
};

export default NewArena;
