// Shows arena-scale information about a single arena.

import React from "react";
import styles from "./styles.css";
import DeleteArenaContainer from "containers/DeleteArenaContainer";

class ArenaHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        arenaId: React.PropTypes.number.isRequired,
        ownclass: React.PropTypes.string.isRequired,
        date: React.PropTypes.number.isRequired,
        wins: React.PropTypes.number.isRequired,
        losses: React.PropTypes.number.isRequired
    };

    renderDate = date => date.getFullYear() + "-" + ("0"+(date.getMonth()+1)).slice(-2) + "-" + ("0"+(date.getDate())).slice(-2);

    render() {
        const date = new Date(this.props.date);
        return (
            <div className={styles.box + " pure-g"} onClick={this.handleClick}>
                <div className="pure-u-1-2 pure-u-md-1-8">{this.props.ownclass}</div>
                <div className="pure-u-1-2 pure-u-md-1-8">{this.props.wins}-{this.props.losses}</div>
                <div className="pure-u-1-2 pure-u-md-1-8">
                    <DeleteArenaContainer
                        arenaId={this.props.arenaId}
                    />
                </div>
                <div className={styles.date + " pure-u-1-2 pure-u-md-5-8"}>{this.renderDate(date)}</div>
            </div>
        );
    }
}

export default ArenaHeader;
