// Shows a button to add a new arena game.

import React from "react";
import styles from "../styles.css";

const NewArenaGame = (props) => {
    return (
        <div className="pure-u-1 pure-u-sm-1-5">
            <button className={styles.box + " pure-button"} onClick={props.handleClick}>
                Add Game
                {props.children}
            </button>
        </div>
    );
};

NewArenaGame.propTypes = {
    handleClick: React.PropTypes.func,
    children: React.PropTypes.object
};

export default NewArenaGame;
