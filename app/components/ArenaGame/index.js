// Shows a single arena game result.

import React from "react";
import styles from "./styles.css";
import commonStyles from "app/common.css";

const ArenaGame = (props) => {
    return (
        <div className="pure-u-1 pure-u-sm-1-5">
            <div className={styles.box + " " + commonStyles.clickable} onClick={props.handleClick}>
                <div className={styles.description}>
                    ({props.opponentclass}, {props.win ? "win" : "lose"}, {props.wentfirst ? "1st" : "2nd"})
                </div>
                {props.children}
            </div>
        </div>
    );
};

ArenaGame.propTypes = {
    opponentclass: React.PropTypes.string.isRequired,
    win: React.PropTypes.bool.isRequired,
    wentfirst: React.PropTypes.bool.isRequired,
    handleClick: React.PropTypes.func,
    children: React.PropTypes.object
};

export default ArenaGame;
