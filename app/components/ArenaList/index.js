// A component displaying a list of completed/progress arenas.

import React from "react";
import ArenaContainer from "containers/ArenaContainer";
import NewArenaContainer from "containers/NewArenaContainer";
import Console from "app/helpers/Console";

const ArenaList = (props) => {
    Console.log("arenas:", props.arenas);
    return (
        <div>
            {props.isFirstPage ? <NewArenaContainer /> : undefined}
            {props.arenas.map((arena) => (<ArenaContainer key={arena.id} {...arena}/>))}
        </div>
    );
};

ArenaList.propTypes = {
    arenas: React.PropTypes.array.isRequired,
    isFirstPage: React.PropTypes.bool.isRequired
};

export default ArenaList;
