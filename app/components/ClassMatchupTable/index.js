import React from "react";
import styles from "./styles.css";
import classNames from "classnames";

const CLASSES = ["mage", "warlock", "priest", "paladin", "hunter", "warrior", "shaman", "druid", "rogue"];

// Data creators

const ownClassTotalData = (matchups) => (
    CLASSES.reduce((data, classname) => [data[0] + matchups[classname][0], data[1] + matchups[classname][1]], [0,0])
);

const oppClassTotalData = (oppclass, allMatchups) => (
    CLASSES.reduce((data, classname) => [data[0] + allMatchups[classname][oppclass][0], data[1] + allMatchups[classname][oppclass][1]], [0,0])
);

// DOM generators

const statContent = (data) => (
    <div>
        <div>
            <span className={styles.wins}>{data[0]}</span>:<span className={styles.losses}>{data[1]}</span>
        </div>
        <div>
            <span className={styles.winRatio}>
                {data[0]+data[1] > 0 ? (data[0]/(data[0]+data[1])*100).toFixed(1) : "-"}%
            </span>
        </div>
    </div>
);

const ownClassLabelCell = (
    <th className={classNames(styles.ownClassLabel, styles.nonContentCell)} rowSpan={9}>
        <div className={styles.verticalText}>
            <div className={styles.verticalTextInner}>
                Your class
            </div>
        </div>
    </th>
);

const ClassHeaderCell = (className) => (
    <th key={className}>
        {className}
    </th>
);

const classCell = (ownClass, oppClass, data) => (
    <td className={styles.dataCell} key={ownClass + "," + oppClass}>
        {statContent(data)}
    </td>
);

const summaryCell = (key, data) => (
    <td className={styles.dataCell} key={key}>
        {statContent(data)}
    </td>
);

const classRow = (className, classMatchups, isFirstRow) => (
    <tr key={className}>
        {isFirstRow? ownClassLabelCell:undefined}
        {ClassHeaderCell(className)}
        {CLASSES.map(c => classCell(className, c, classMatchups[c]))}
        {summaryCell(className + "Summary", ownClassTotalData(classMatchups))}
    </tr>
);

const oppClassSummaryRow = (allMatchups) => (
    <tr>
        <td className={styles.nonContentCell}></td>
        <td className={styles.nonContentCell}></td>
        {CLASSES.map(c => summaryCell(c, oppClassTotalData(c, allMatchups)))}
        <td className={styles.nonContentCell}></td>
    </tr>
);

const ClassMatchupTable = (props) => {
    const data = props.data;
    if (data) {

        return (
            <table className={styles.classMatchupTable}>
                <thead>
                    <tr>
                        <th className={styles.nonContentCell}></th>
                        <th className={styles.nonContentCell}></th>
                        <th className={styles.nonContentCell} colSpan={9}>
                            Opponent class
                        </th>
                        <th className={styles.nonContentCell}></th>
                    </tr>
                    <tr>
                        <th className={styles.nonContentCell}></th>
                        <th className={styles.nonContentCell}></th>
                        {CLASSES.map(c => ClassHeaderCell(c))}
                        <th className={styles.nonContentCell}></th>
                    </tr>
                </thead>
                <tbody>
                    {CLASSES.map((c, index) => classRow(c, data[c], (index===0)))}
                    {oppClassSummaryRow(data)}
                </tbody>
            </table>
        );
    }
    else {
        return <div></div>;
    }
};

ClassMatchupTable.propTypes = {
    data: React.PropTypes.object
};

export default ClassMatchupTable;
