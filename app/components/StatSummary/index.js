// Shows the current user"s stats (total games played, average, etc)

import React from "react";

const StatSummary = (props) => {
    return (
        <div>
            <p>Arena stats</p>
            <p>Total: {props.stats.total} Average wins: {props.stats.average.all.toFixed(3)} (Optimal: {props.stats.average.optimal.toFixed(3)})</p>
        </div>
    );
};

StatSummary.propTypes = {
    stats: React.PropTypes.object.isRequired
};

export default StatSummary;
