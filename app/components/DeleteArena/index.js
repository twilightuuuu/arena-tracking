import React from "react";
import commonStyles from "app/common.css";

const DeleteArena = (props) => {
    return (
        <i className={commonStyles.clickable + " fa fa-trash-o"} onClick={props.handleClick}>
            {props.children}
        </i>
    );
};

DeleteArena.propTypes = {
    handleClick: React.PropTypes.func,
    children: React.PropTypes.object
};

export default DeleteArena;
