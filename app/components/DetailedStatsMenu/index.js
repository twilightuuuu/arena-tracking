import React from "react";
import MenuLink from "components/MenuLink";
import styles from "./styles.css";
import classNames from "classnames";

const DetailedStatsMenu = (props) => {
    return (
        <div className={classNames("pure-menu", "pure-menu-horizontal", "pure-menu-scrollable", styles.box)}>
            <ul className="pure-menu-list">
                <MenuLink
                    to="/stats/vs-class"
                    label="VS Class"
                    isActive={props.currentRoute === "/stats/vs-class"}
                />
            </ul>
        </div>
    );
};

DetailedStatsMenu.propTypes = {
    currentRoute: React.PropTypes.string.isRequired
};

export default DetailedStatsMenu;
