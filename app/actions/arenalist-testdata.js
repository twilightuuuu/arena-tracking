export default {
    "arenas": [
        {
            "id": 1,
            "date":1481775283647,
            "ownclass": "mage",
            "games": [
                {
                    "opponentclass": "paladin",
                    "win": true,
                    "wentfirst": false
                },
                {
                    "opponentclass": "rogue",
                    "win": false,
                    "wentfirst": true
                }
            ]
        },{
            "id": 0,
            "date":1481775283647,
            "ownclass": "priest",
            "games": [
                {
                    "opponentclass": "paladin",
                    "win": false,
                    "wentfirst": false
                },
                {
                    "opponentclass": "mage",
                    "win": true,
                    "wentfirst": true
                },{
                    "opponentclass": "druid",
                    "win":false,
                    "wentfirst":false
                },{
                    "opponentclass": "mage",
                    "win": true,
                    "wentfirst": true
                },{
                    "opponentclass": "warrior",
                    "win":false,
                    "wentfirst":true
                }
            ]
        }
    ],
    "total":2
};
