// Actions from the ArenaList page.

import * as ArenaData from "app/helpers/ArenaData";
import ActionTypes from "app/actiontypes";
import Console from "app/helpers/Console";

function loadArenaList(arenaListState) {
    const data = ArenaData.getArenaList(
        arenaListState.currentPage, 
        arenaListState.arenasPerPage);
    Console.log("ArenaList:",arenaListState);
    return {
        type: ActionTypes.ArenaList.LOAD,
        data
    };
}

export function loadInitialArenaList() {
    return (dispatch, getState) => {
        dispatch(loadArenaList(getState().ArenaList));
    };
}

export function addArena(arena) {
    Console.log("addArena", arena);
    ArenaData.addArena(arena);
    return (dispatch, getState) => {
        dispatch(loadArenaList(getState().ArenaList));
    };
}

export function deleteArena(arenaId) {
    Console.log("deleteArena", arenaId);
    ArenaData.deleteArena(arenaId);
    return (dispatch, getState) => {
        dispatch(loadArenaList(getState().ArenaList));
    };

}

export function addGame(arenaId, game) {
    Console.log("addGame", arenaId);
    Console.log(game);
    ArenaData.addGame(arenaId, game);
    return (dispatch, getState) => {
        dispatch(loadArenaList(getState().ArenaList));
    };

}

export function editGame(arenaId, gameIndex, game) {
    Console.log("editGame", arenaId, gameIndex);
    Console.log(game);
    ArenaData.editGame(arenaId, gameIndex, game);
    return (dispatch, getState) => {
        dispatch(loadArenaList(getState().ArenaList));
    };

}

export function removeGame(arenaId, gameIndex) {
    Console.log("removeGame", arenaId, gameIndex);
    ArenaData.removeGame(arenaId, gameIndex);
    return (dispatch, getState) => {
        dispatch(loadArenaList(getState().ArenaList));
    };

}

export function setCurrentPage(currentPage) {
    Console.log("setCurrentPage", currentPage);
    return (dispatch, getState) => {
        dispatch({
            type: ActionTypes.ArenaList.SET_CURRENT_PAGE,
            currentPage
        });
        dispatch(loadArenaList(getState().ArenaList));
    };
}
