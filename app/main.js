import React from "react";
import ReactDOM from "react-dom";
import configureStore from"./store/configureStore";
import RootContainer from "containers/RootContainer";

ReactDOM.render(
    <RootContainer 
        store={configureStore(undefined)}/>,
    document.getElementById("root"));
