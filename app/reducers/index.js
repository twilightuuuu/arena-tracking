import ArenaListReducer from "app/reducers/ArenaListReducer";
import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";

export default combineReducers({
    ArenaList: ArenaListReducer,
    routing: routerReducer
});
