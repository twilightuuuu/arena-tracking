// A reducer handling changes to the ArenaList state.

import ActionTypes from "app/actiontypes";
import Console from "app/helpers/Console";

const initialState = {
    arenas: [],
    currentPage: 0,
    arenasPerPage: 5,
    graphData: [],
    stats: {
        total: 0,
        average: {
            all: 0,
            optimal: 0
        }
    }
};

function deepCopyArenas(arenas) {
    return arenas.map(arena => {
        return Object.assign({}, arena, {
            games: arena.games.map(game => {
                return Object.assign({}, game);
            })
        });
    });
}

function newState(state, data) {
    return {
        ...initialState,
        ...state,
        ...data
    };
}

function addArena(state, arena) {
    const nowDate = Date.now();
    Console.log(nowDate);
    let arenas = deepCopyArenas(state.arenas);
    arenas.splice(0, 0, {
        id: arenas.length > 0 ? arenas[0].id + 1 : 0,
        date: nowDate,
        ownclass: arena.ownclass,
        games: []
    });

    return {
        ...state,
        arenas,
    };
}

function deleteArena(state, arenaId) {
    let arenas = deepCopyArenas(state.arenas).filter(arena => arena.id !== arenaId);

    return {
        ...state,
        arenas,
    };
}

function addGame(state, arenaId, newGame) {
    let arenas = deepCopyArenas(state.arenas).map(arena => {
        if (arena.id === arenaId) {
            arena.games.push(newGame);
        }
        return arena;
    });

    return {
        ...state,
        arenas
    };
}

function editGame(state, arenaId, gameIndex, newGame) {
    let arenas = deepCopyArenas(state.arenas).map(arena => {
        if (arena.id === arenaId) {
            arena.games[gameIndex] = newGame;
        }
        return arena;
    });

    return {
        ...state,
        arenas
    };
}

function removeGame(state, arenaId, gameIndex) {
    let arenas = deepCopyArenas(state.arenas).map(arena => {
        if (arena.id === arenaId) {
            arena.games.splice(gameIndex, 1);
        }
        return arena;
    });

    return {
        ...state,
        arenas
    };
}

function setCurrentPage(state, currentPage) {
    return Object.assign({}, state, {currentPage});
}

export default (state=initialState, action) => {
    switch(action.type) {
        case ActionTypes.ArenaList.LOAD:
            return newState(state, action.data);
        case ActionTypes.ArenaList.ADD_ARENA:
            return addArena(state, action.arena);
        case ActionTypes.ArenaList.REMOVE_ARENA:
            return deleteArena(state, action.arenaId);
        case ActionTypes.ArenaList.ADD_GAME:
            return addGame(state, action.arenaId, action.game);
        case ActionTypes.ArenaList.EDIT_GAME:
            return editGame(state, action.arenaId, action.gameIndex, action.game);
        case ActionTypes.ArenaList.REMOVE_GAME:
            return removeGame(state, action.arenaId, action.gameIndex);
        case ActionTypes.ArenaList.SET_CURRENT_PAGE:
            return setCurrentPage(state, action.currentPage);
    }

    return state;
};
