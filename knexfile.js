// Update with your config settings.

module.exports = {

    test: {
        client: "sqlite3",
        connection: {
            filename: "./test.sqlite3"
        },
        migrations: {
            tableName: "knex_migrations",
            directory: "./server/migrations"
        }
    },

    development: {
        client: "sqlite3",
        debug: true,
        connection: {
            filename: "./development.sqlite3"
        },
        migrations: {
            tableName: "knex_migrations",
            directory: "./server/migrations"
        }
    },

    staging: {
        client: "postgresql",
        connection: {
            database: "my_db",
            user:     "username",
            password: "password"
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: "knex_migrations"
        }
    },

    production: {
        client: "sqlite3",
        connection: {
            filename: "./production.sqlite3"
        },
        migrations: {
            tableName: "knex_migrations",
            directory: "./server/migrations"
        }
    }

};
