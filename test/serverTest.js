// Tests server interactions (mainly apis).

const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
const server = require("../server");
const knex = require("../server/db").knex;
const moment = require("moment");
const checkResponseMessage = require("../testHelpers/checkResponseMessage");

chai.use(chaiHttp);

const resetUsersDB = function(done) {
    knex("users").del()
        .then(function() {
            done();
        });
};

const resetArenasDB = function(done) {
    knex("arenas").del()
        .then(function() {
            done();
        });
};

describe("DB sanity", function() {
    describe("FK", function() {
        beforeEach(resetArenasDB);
        after(resetArenasDB);

        it("INSERTs violating FK constraints should not work", function(done) {
            knex("arenas").insert({
                userId: 5,
                startDate: moment().format("YYYY/MM/DD HH:mm:ss"),
                category: 3,
                ownPlayerClass: 999
            }).then(function() {
                knex("arenas").select().then(function(rows) {
                    console.log(rows);
                });
                done("Insert went through without errors");
            }).catch(function(err) {
                done();
            });
        });
    });
});

describe("Users", function() {
    describe("registering", function() {
        beforeEach(resetUsersDB);
        after(resetUsersDB);

        it("should return 500 status if the required parameters are missing", function(done) {
            chai.request(server)
                .post("/api/user/register")
                .send({})
                .end(function(err, res) {
                    checkResponseMessage(res, 500);
                    done();
                });
        });

        it("should return a success response on POST /api/user/register", function(done) {
            chai.request(server)
                .post("/api/user/register")
                .send({
                    username: 'tester',
                    password: 'password'
                })
                .then(function(res) {
                    checkResponseMessage(res, 200, "ok");
                    done();
                })
                .catch(function(err) {
                    done(err);
                });
        });

        describe("on existing username", function() {
            beforeEach(function(done) {
                chai.request(server)
                    .post("/api/user/register")
                    .send({
                        username: "tester",
                        password: "password"
                    })
                    .end(function(err, res) {
                        checkResponseMessage(res, 200, "ok");
                        done();
                    });
            });

            it("should return an error response", function(done) {
                chai.request(server)
                    .post("/api/user/register")
                    .send({
                        username: 'tester',
                        password: 'password2'
                    })
                    .end(function(err, res) {
                        checkResponseMessage(res, 400, "Username already exists");
                        done();
                    });
            });
        });

        it("should return an error response if the username is too short", function(done) {
            chai.request(server)
                .post("/api/user/register")
                .send({
                    username: 'asd',
                    password: 'pasdf'
                })
                .end(function(err, res) {
                    checkResponseMessage(res, 400, "Username must be at least 4 characters");
                    done();
                });
        });
    });

    describe("login", function() {
        before(resetUsersDB);
        after(resetUsersDB);

        it("should return 500 status if the required parameters are missing", function(done) {
            chai.request(server)
                .post("/api/user/login")
                .send({})
                .end(function(err, res) {
                    checkResponseMessage(res, 500);
                    done();
                });
        });

        describe("after registering", function() {
            before(function(done) {
                chai.request(server)
                    .post("/api/user/register")
                    .send({
                        username: "test",
                        password: "password"
                    })
                    .end(function(err, res) {
                        checkResponseMessage(res, 200, "ok");
                        done();
                    });
            });
            after(resetUsersDB);

            it("should successfully login with correct info", function(done) {
                chai.request(server)
                    .post("/api/user/login")
                    .send({
                        username: "test",
                        password: "password"
                    })
                    .end(function(err, res) {
                        checkResponseMessage(res, 200, "ok");
                        expect(res.body).to.contain.all.keys("username");
                        expect(res.body.username).to.equal("test");
                        done();
                    });
            });

            it("should not be able to login with incorrect info", function(done) {
                chai.request(server)
                    .post("/api/user/login")
                    .send({
                        username: "test",
                        password: "wrongpass"
                    })
                    .end(function(err, res) {
                        checkResponseMessage(res, 400, "User not found");
                        done();
                    });
            });
        });

        describe("authentication", function() {
            var agent;

            // Register a user.
            before(function(done) {
                chai.request(server)
                    .post("/api/user/register")
                    .send({
                        username: "test",
                        password: "password"
                    })
                    .end(function(err, res) {
                        checkResponseMessage(res, 200, "ok");
                        done();
                    });
            });

            // Authenticate the agent.
            before(function(done) {
                agent = chai.request.agent(server);
                agent.post("/api/user/login")
                    .send({
                        username: "test",
                        password: "password"
                    })
                    .end(function(err, res) {
                        checkResponseMessage(res, 200, "ok");
                        expect(res.body.username).to.equal("test");
                        done();
                    });
            })

            after(resetUsersDB);

            it("should be authenticated after login", function(done) {
                agent.get("/api/user/isLogged")
                    .end(function(err, res) {
                        checkResponseMessage(res, 200, "ok");
                        done();
                    });
            });

            it("should not be authenticated before login", function(done) {
                chai.request(server)
                    .get("/api/user/isLogged")
                    .end(function(err, res) {
                        checkResponseMessage(res, 400, "Not logged in");
                        done();
                    });
            });
        });

    });
});
