// Webpack settings that are common to both dev/production builds.
'use strict';
var path = require('path');

module.exports = {
    resolve: {
        alias: {
            components: path.join(__dirname, 'app/components'),
            containers:path.join(__dirname, 'app/containers'),
            app: path.join(__dirname, 'app'),
            server: path.join(__dirname, 'server')
        }
    }
};
