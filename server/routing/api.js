// Connects API requests to the functions.
// All API requests start with /api/.

const router = require("express").Router();
const userApi = require("../api/user");
const bodyParser = require("body-parser");
const errorHandler = require("../errors/handler");
const passport = require("passport");
const ValidationError = require("../errors/ValidationError");

router.use(bodyParser.json());

// Registers a user with the given info.
router.post("/user/register", (req, res) => {
    userApi.addUser(req)
        .then(() => {
            res.status(200).json({status:"ok"});
        })
        .catch(err => {
            errorHandler(err, res);
        });
});

// Logs the given user in, if the creditentals are correct.
router.post("/user/login", (req, res) => {
    if (!req.body.username || !req.body.password) {
        errorHandler("required parameters missing", res);
    }
    else {
        passport.authenticate("local", (err, user, info) => {
            if (err) {
                errorHandler(err, res);
            }

            if (user) {
                // Since we invoked authenticate() ourselves, we also have to call login().
                req.login(user, (err) => {
                    res.status(200).json({
                        status:"ok",
                        username:user.username,
                        createdDate:user.createdDate
                    });
                });
            }
            else {
                res.status(400).json({status:"User not found"});
            }
        })(req, res);
    }
});

// Checks whether the request is authenticated.
router.get("/user/isLogged", ensureAuth, (req, res) => {
    res.status(200).json({
        status:"ok",
        username: req.user.username
    });
});

function ensureAuth(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    else {
        errorHandler(new ValidationError("Not logged in"), res);
    }
}

module.exports = router;
