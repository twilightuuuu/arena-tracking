// Initializes a db connection, and exports the connector object.

const sqlite3 = require("sqlite3");
if (process.env.NODE_ENV !== "production") {
    sqlite3.verbose();
}

const dbPath = "./" + process.env.NODE_ENV + ".sqlite3";

const db = new sqlite3.Database(dbPath);

// Initialize knex.
const knexConfig = require("../knexfile");
const knex = require("knex")(knexConfig[process.env.NODE_ENV]);
knex.raw("PRAGMA foreign_keys = true")
    .then(() => {
        // For now, we'll put another query here to
        // make the foreign key setting finish before
        // other queries execute.
        // This should be better than async module loading...
        knex.raw("PRAGMA foreign_keys")
            .then(rows => {
            });
    });

module.exports = {db, knex};
