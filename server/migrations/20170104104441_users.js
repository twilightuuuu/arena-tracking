exports.up = function(knex, Promise) {
    return knex.schema.createTable("users", function (table) {
        table.increments("id").unsigned().primary();
        table.string("username").unique();
        table.string("password", 60);
        table.datetime("createdDate");
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable("users");
};
