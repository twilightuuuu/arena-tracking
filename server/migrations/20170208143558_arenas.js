exports.up = function(knex, Promise) {
    return knex.raw("PRAGMA foreign_keys = true")
        .then(() => knex.schema.createTable("arenas", function(table) {
            table.increments("id").unsigned().primary();
            table.integer("userId").unsigned().index();
            table.datetime("startDate");
            table.integer("category").unsigned().index();
            table.integer("ownPlayerClass").unsigned();

            table.foreign("userId")
                .references("users.id")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
        }))
        .then(() => knex.schema.createTable("games", function(table) {
            table.increments("id").unsigned().primary();
            table.integer("arenaId").unsigned().index();
            table.datetime("date");
            table.integer("oppPlayerClass").unsigned();
            table.boolean("wentFirst");
            table.boolean("won");

            table.foreign("arenaId")
                .references("arenas.id")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
        }));
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable("games")
        .then(() => knex.schema.dropTable("arenas"));
};
