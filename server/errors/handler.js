// Error handler to use for all routing functions.
const ValidationError = require("./ValidationError");

const handler = (error, response) => {
    if (error instanceof ValidationError) {
        response.status(400).json({
            status: error.reason
        });
    }
    else if (error instanceof Error) {
        console.log("Error catched:" + error);
        response.status(500).end();
    }
    else {
        console.log("Non-Error catched:" + error);
        response.status(500).end();
    }
};

module.exports = handler;
