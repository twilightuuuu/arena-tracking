// Custom error type to be used on input validation errors.

const ValidationError = function(reason) {
    this.reason = reason;
    Error.captureStackTrace(this, ValidationError);
};

ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.constructor = ValidationError;

module.exports = ValidationError;
