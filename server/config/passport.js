// Passport-related configs.
const knex = require("../db").knex;
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcryptjs");

module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        knex("users").where({id}).first()
            .then((user) => { done(null, user); })
            .catch((err) => { done(err); });
    });

    passport.use(new LocalStrategy(
        function(username, password, done) {
            knex("users").where({username}).select()
                .then((users) => {
                    if (users.length > 0
                        && bcrypt.compareSync(password, users[0].password)) {
                        done(null, users[0]);
                    } else {
                        done(null, false);
                    }
                })
                .catch((err) => { done(err); });
        }
    ));
};
