// Arena-related API functions.
// Some functions require login data.
const knex = require("../db").knex;
const ValidationError = require("../errors/ValidationError");

// Exposed API functions

const getArenaData = (req) => {
    return validateUser(req)
        .then(() => getArenaData(req));
};

// Internal functions

const validateUser = (req) => {
    return new Promise((resolve, reject) => {
        if (!req.user || req.user.id === undefined) {
            reject(new ValidationError("Invalid user info"));
        }

        resolve();
    });
};

const parsePageNum = (req) => {
    return new Promise((resolve, reject) => {
        const pageNum = parseInt(req.body.pageNum);
        if (!Number.isInteger(pageNum) || pageNum < 0) {
            reject(new ValidationError("Incorrect page # format"));
        }

        resolve(pageNum);
    });
};

const getArenaData = (req, {pageNum}) => {
    const query = knex("arenas")
        .leftOuterJoin("games", "arenas.id", "games.arenaId")
        .select(
            "arenas.id as arenaId",
            "arenas.startDate as arenaDate",
            "arenas.ownPlayerClass",
            "games.id as gameId",
            "games.date as gameDate",
            "games.oppPlayerClass",
            "games.wentFirst",
            "games.won"
        );
};

export {
    getArenaPage
};
