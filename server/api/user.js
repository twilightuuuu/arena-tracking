// User-related API functions.
const bcrypt = require("bcryptjs");
const knex = require("../db").knex;
const ValidationError = require("../errors/ValidationError");
const passport = require("passport");
const moment = require("moment");

const addUser = (req) => {
    return validateRegisterInfo(req)
        .then(() => addUserToDB(req));
};

const validateRegisterInfo = (req) => {
    return new Promise((resolve, reject) => {
        if (req.body.username.length < 4) {
            reject(new ValidationError("Username must be at least 4 characters"));
        }

        if (req.body.password.length < 1) {
            reject(new ValidationError("Password must be at least 1 character"));
        }

        knex("users")
            .first()
            .where({
                username: req.body.username
            })
            .then(result => {
                if (result) {
                    reject(new ValidationError("Username already exists"));
                }
            })
            .then(() => {resolve();});
    });
};

const addUserToDB = (req) => {
    const salt = bcrypt.genSaltSync();
    const hash = bcrypt.hashSync(req.body.password, salt);
    return knex("users").insert({
        username: req.body.username,
        password: hash,
        createdDate: moment().format("YYYY/MM/DD HH:mm:ss")
    });
};

module.exports = {
    addUser
};
