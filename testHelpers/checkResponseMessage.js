// A wrapper function for checking responses with chai.

const chai = require("chai");
const expect = chai.expect;

const checkResponseMessage = function(response, statusCode, expectedMessage) {
    expect(response).to.have.status(statusCode);
    if (expectedMessage) {
        expect(response).to.be.json;
        expect(response).to.contain.all.keys("body");
        expect(response.body).to.contain.all.keys("status");
        expect(response.body.status).to.equal(expectedMessage);
    }
};

module.exports = checkResponseMessage;
